from django.db import models


class Semester(models.Model):
	WINTER = 1
	SUMMER = 2
	SEASON_CHOICES = (
	        (WINTER, 'zima'),
	        (SUMMER,  'lato')
	)
	year = models.PositiveIntegerField()
	season = models.PositiveIntegerField(choices=SEASON_CHOICES)

	@classmethod
	def last_semester(cls):
		return cls.objects.reverse()[0]

	class Meta:
		ordering = ['year', 'season']

	def __unicode__(self):
		return str(self.year) + '/' + str(self.year+1) + ' ' + self.get_season_display()


class Task(models.Model):
	name = models.CharField(max_length=32)
	full_name = models.CharField(max_length=200)
	url = models.CharField(max_length=1024)
	date = models.DateField()
	visible = models.BooleanField(default=True)
	semester = models.ForeignKey(Semester)
	
	def add_solution(self, student):
		Solution(task=self, solved_by=student).save()
	
	def __unicode__(self):
		return self.full_name


class Student(models.Model):
	name = models.CharField(max_length=256)
	surname = models.CharField(max_length=256)
	solved = models.ManyToManyField(Task, through='Solution')
	
	def points_for_semester(self, semester):
		points = 0
		for task in semester.task_set.filter(visible=True):
			if self.solved_task(task):
				if semester.id==1:
					points += 1 # old system
				else:
					denominator = task.student_set.count()
					points += 60.0/denominator
		return int(points)
	
	def solved_task(self, task):
		return bool(self.solved.filter(id=task.id))
	
	def __unicode__(self):
		if Student.objects.filter().count() > 1 and self.surname:
			return self.name + " " + self.surname[0] + "."
		else:
			return self.name


class Solution(models.Model):
	task = models.ForeignKey(Task)
	solved_by = models.ForeignKey(Student)
	
	def save(self, *args, **kwargs):
		if Solution.objects.filter(task=self.task, solved_by=self.solved_by).count():
			raise Exception, "solutions should be unique"
		super(Solution, self).save() 
	
	def __unicode__(self):
		return "("+unicode(self.task)+" , "+unicode(self.solved_by)+")"

