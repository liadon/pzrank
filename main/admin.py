from main.models import *
from django.contrib import admin

admin.site.register(Student)
admin.site.register(Task)
admin.site.register(Solution)
admin.site.register(Semester)
