from django.shortcuts import render, get_object_or_404, redirect
from main.models import Task, Student, Solution, Semester
from operator import attrgetter
import sys
import traceback

def ranktable_for_semester(request,semester_id):
	semester = get_object_or_404(Semester,id=semester_id)
	semesters = Semester.objects.all()

	tasks = sorted(semester.task_set.filter(visible=True), key=attrgetter('date'), reverse=1)
	students = set()
	for task in tasks:
		students |= set(task.student_set.all())
	for student in students:
		student.points = student.points_for_semester(semester)
	students = sorted(students, key=attrgetter('points'), reverse=1)

	taskdates = []
	for task in tasks:
		if not taskdates or task.date != taskdates[-1][0]:
			taskdates.append((task.date, []))
		solutions = [ (student, student.solved_task(task)) for student in students ]
		taskdates[-1][1].append((task, solutions)) 
		
	return render(request,"ranktable.html", {
		'students' : students,
		'taskdays' : taskdates,
		'semesters' : semesters,
		'showed_semester' : semester,
	})

def ranktable(request):
	return ranktable_for_semester(request,Semester.last_semester().id)

def task_details(request,task_id):
	task = get_object_or_404(Task,id=task_id)
	return render(request,"task_details.html", {
		'task' : task
	})

def student_details(request,student_id):
	student = get_object_or_404(Student,id=student_id)

	solved_by_semester = []
	for semester in Semester.objects.reverse():
		solved = student.solved.filter(semester=semester)
		if solved:
			unsolved = Task.objects.filter(semester=semester
				).exclude(id__in = solved.values_list('id', flat=True))
			
			unsolved = sorted(set(unsolved), key=lambda task: -len(Solution.objects.filter(task=task)))


			solved_by_semester.append({
				'semester' : semester,
				'solved' : solved,
				'unsolved' : unsolved,
				'points' : student.points_for_semester(semester)
			})

	return render(request,"student_details.html", {
		'student' : student,
		'niceness' : Student.objects.count() - student.id + 1,
		'solved_by_semester' : solved_by_semester
	})

def add_solution(request, task_id, student_id):
	task = get_object_or_404(Task, id=task_id)
	student = get_object_or_404(Student, id=student_id)
	task.add_solution(student)
	return redirect("/")


def server_error_verbose(request, template_name='verbose500.html'):
	ltype,lvalue,ltraceback = sys.exc_info()
	ltraceback = traceback.extract_tb(ltraceback)
	sys.exc_clear()
	
	return render(request, template_name, {
		'type' : unicode(ltype),
		'value' : lvalue,
		'traceback' : ltraceback
	}, status=500)
	
