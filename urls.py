from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'views.home', name='home'),
    # url(r'^rank/', include('rank.foo.urls')),
    url(r'^$','main.views.ranktable'),
    url(r'^task/(?P<task_id>\d+)/$','main.views.task_details', name='task'),
    url(r'^student/(?P<student_id>\d+)/$','main.views.student_details', name='student'),
    url(r'^semester/(?P<semester_id>\d+)/$','main.views.ranktable_for_semester', name='semester'),
    url(r'^add_solution/(?P<task_id>\d+)/(?P<student_id>\d+)/$','main.views.add_solution', name='add_solution'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

#handler500 = 'main.views.server_error_verbose'